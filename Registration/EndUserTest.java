package com.seleniumtests.tests;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.seleniumtests.core.Filter;
import com.seleniumtests.core.SeleniumTestPlan;
import com.seleniumtests.dataobject.EndUserData;
import com.seleniumtests.util.SpreadSheetHelper;
import com.seleniumtests.util.internal.entity.TestEntity;
import com.seleniumtests.webpage.EndUserPage;

public class EndUserTest extends SeleniumTestPlan{

	@DataProvider(

			name = "EndUserData",
			parallel = false
			)
			public static Iterator < Object[] > getUserInfo(final Method m) throws Exception {


			Filter filter = Filter.equalsIgnoreCase(TestEntity.TEST_METHOD, m.getName());


			LinkedHashMap < String, Class < ? >> classMap = new LinkedHashMap < String, Class < ? >> ();
			classMap.put("TestEntity", TestEntity.class);
			classMap.put("EndUserData", EndUserData.class);

			return SpreadSheetHelper.getEntitiesFromSpreadsheet(EndUserTest.class, classMap, "c1.csv", filter);
			}
			
			@Test(
		            groups = { "sanity", "regression" },
		            dataProvider = "EndUserData",
		            description = "Verify the All content subscription plan in CMS"
		        )
		    
		        public void TC_Reg_01(final TestEntity testEntity,final EndUserData EndUserData) throws Exception {

		            new EndUserPage(true).TC_Register_01(EndUserData);
		            
		        }
	
}
