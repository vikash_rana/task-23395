/**
 * @author  Vikash Rana <vikash@muvi.com>
 */

package com.seleniumtests.webpage;

import static com.seleniumtests.core.Locator.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.ContentData;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;
import static com.seleniumtests.core.CustomAssertion.*;
import static com.seleniumtests.core.Locator.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.lang.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.github.javafaker.Faker;
import com.google.gdata.util.parser.Action;
import com.seleniumtests.apicontroller.RestExecutor;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.DbManager;
import com.seleniumtests.dataobject.DbManager2;
import com.seleniumtests.dataobject.EndUserData;
import com.seleniumtests.dataobject.BillingData;
import com.seleniumtests.dataobject.ContentData;
import com.seleniumtests.dataobject.RandomAddress;
import com.seleniumtests.dataobject.RandomEmailID;
import com.seleniumtests.dataobject.RandomMobileNo;
import com.seleniumtests.dataobject.RandomName;
import com.seleniumtests.dataobject.RandomPassword;
import com.seleniumtests.dataobject.RandomZip;
import com.seleniumtests.dataobject.WebCRMData;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.Table;
import com.seleniumtests.webelements.TextFieldElement;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.GetCurrentUrl;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mysql.jdbc.Driver;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.ContentData;

import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;

public class EndUserPage extends PageObject {
 public static boolean booleanvalue = false;

 public static String url = null;
 public static String URL = null;
 private static final String APIURL = "https://mailsac.com/api/addresses/";
 public static Map < Object, Map < Object, Object >> mastermap = new HashMap < Object, Map < Object, Object >> ();
 public EndUserPage(final boolean openPageURL) throws Exception {
  super(htmlpage, openPageURL ? url = SeleniumTestsContextManager.getThreadContext().getAppURL() : null);
 }

 private static final ButtonElement htmlpage = new ButtonElement("Click login button ",
		  locateByXPath("//html"));
 
 private static final ButtonElement LoginButton = new ButtonElement("Click login button ",
		  locateByXPath("//a[@class='_login']"));
 private static final TextFieldElement LoginUserNameTextbox = new TextFieldElement("enter username in User name textbox ",
		  locateByXPath("//input[@id='LoginForm_email']"));
 private static final TextFieldElement PasswordNameTextbox = new TextFieldElement("enter password in password textbox ",
		  locateByXPath("//input[@id='LoginForm_password']"));
 private static final ButtonElement LoginButton1 = new ButtonElement("Click login button ",
		  locateByXPath("//input[@id='btn-login']"));
 
 private static final ButtonElement PreviewButton = new ButtonElement("Click Preview from CMS ",
	       locateByXPath("//a[contains(text(),'Preview Website')]"));
 
 private static final ButtonElement RegisterNav = new ButtonElement("Click Register from Frontend ",
		  locateByXPath("//a[@class='register muvi_c-link']"));
 private static final TextFieldElement EndUserNameTextbox = new TextFieldElement("enter username in User name textbox ",
		  locateByXPath("//form[@id='membership_form']//input[@id='name']"));
 private static final TextFieldElement EndUserEmailTextbox = new TextFieldElement("enter email in User email textbox ",
		  locateByXPath("//form[@id='membership_form']//input[@id='email_address']"));
 private static final TextFieldElement EndUserPassTextbox = new TextFieldElement("enter password in password textbox ",
		  locateByXPath("//form[@id='membership_form']//input[@id='join_password']"));
 private static final TextFieldElement EndUserCnfmTextbox = new TextFieldElement("enter password in Confirm password textbox ",
		  locateByXPath("//form[@id='membership_form']//input[@id='confirm_password']"));
 private static final ButtonElement RegisterButton = new ButtonElement("Click Register button ",
		  locateByXPath("//button[@id='register_membership']"));
 
 
 String ExpMonth = "//select[@id='exp_month']";
 String ExpYear = "//select[@id='exp_year']";
 String Country = "//select[@id='countr']";
 String CurrencyDropdown = "//select[@id='currency-box']";
 
 		
 public EndUserPage Textbox(TextFieldElement element,final String Text) throws Exception {
 	waitForSeconds(2);
 	element.clearAndType(Text);
     return this;
 } 
 public EndUserPage MouseOver(HtmlElement element) throws Exception {
 	
 	element.waitForPresent(10);
 	element.mouseOver();
     return this;
 } 
  

  public EndUserPage Click(ButtonElement element) throws Exception {
 	   
 	   element.waitForPresent(10);
 	   element.click();
         return this;
     }
  public EndUserPage Click1(ButtonElement element) throws Exception {
 	 waitForSeconds(10);
  	try {
 	    element.click();
  	}catch(Exception e) {
  		
  	}
      return this;
  }
  
  public EndUserPage Scroll(ButtonElement element) throws Exception {
 	 waitForSeconds(2);
 	    element.scroll();
      return this;
  }
  
  
  public EndUserPage SimulateClick(ButtonElement element) throws Exception {
 	 waitForSeconds(2);
 	    element.simulateClick();
      return this;
  }
  public EndUserPage HtmlvideoPlayerPlay(HtmlElement element) throws Exception {
 	 waitForSeconds(2);
 	    element.HtmlvideoPlayerPlay();
      return this;
  }
  public EndUserPage HtmlvideoPlayerPause(HtmlElement element) throws Exception {
 	 waitForSeconds(2);
 	    element.HtmlvideoPlayerPause();
      return this;
  }
  
  public EndUserPage selectNewTab() throws Exception {
 	 waitForSeconds(2);
  	selectNewWindow();
 	return this;
 	
      
  }
  
  public EndUserPage SelectWindow(int index) throws Exception {
 	 waitForSeconds(2);
 	 	selectWindow(index);
 		return this;
 	     
 	 }
  
  public EndUserPage printpagesource() throws Exception {
 	 waitForSeconds(2);
 	 	System.out.println(driver.getPageSource());
 		return this;
  }
  public EndUserPage Wait(int value) throws Exception {
 	 waitForSeconds(value);
 	 	
 		return this;
 	     
 	 }
  public EndUserPage WaitforElelment(HtmlElement element) throws Exception {
 	 waitForElementPresent(element);
 		return this;    
 	 }
  public EndUserPage SelectByList(final String Text,String xpath) throws Exception {
 	 waitForSeconds(5);
 	SelectList s = new SelectList(Text, By.xpath(xpath));
 	s.selectByValue(Text);
 		return this;
 	     
 	 }
  public EndUserPage SelectByListIndex(final int index,String xpath) throws Exception {
 	 waitForSeconds(5);
 		SelectList s = new SelectList("", By.xpath(xpath));
 		s.selectByIndex(index);
 			return this;
 	 }
  public EndUserPage AssertEquals(final String actualText,final String expectedText) throws Exception {
		 assertEquals(actualText.toLowerCase(),expectedText.toLowerCase(),"Actual text and Expected Text Not Equal");
	     return this;
	 }
  public EndUserPage AssertFalse(final Boolean actualText) throws Exception {
		 assertFalse(actualText,"Text should not be visible but now its visible");
	     return this;
     }
  public EndUserPage AssertTrue(final Boolean actualText) throws Exception {
		 assertTrue(actualText,"Text should  be visible but now its not visible");
	     return this;
	 }
  public EndUserPage pressEnter() throws Exception {
		 Actions act=new Actions(driver);
		 act.sendKeys(Keys.ENTER).perform();
	     return this;
	 }
  
  public EndUserPage TC_Register_01(final EndUserData EndUserData) throws Exception {
	return Click(LoginButton).Wait(2)
			.Textbox(LoginUserNameTextbox,"Upgrade_muvi@yopmail.com").Wait(2)
			.Textbox(PasswordNameTextbox,"muvi1234#").Wait(2)
			.Click(LoginButton1).Wait(5)
			.Click(PreviewButton).Wait(5)
			.selectNewTab()
			.Click(RegisterNav).Wait(5)
			.Textbox(EndUserNameTextbox, "Muvi_Auto")
			.Textbox(EndUserEmailTextbox, "Muvi_Auto2@yopmail.com")
			.Textbox(EndUserPassTextbox, "123456")
			.Textbox(EndUserCnfmTextbox, "123456")
			.Click(RegisterButton).Wait(5);
	  }
  
}