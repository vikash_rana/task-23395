package com.seleniumtests.dataobject;

public class PPVData {

	private String loginUserName;
	private String loginPassWord;
	private String loginName;
	private String name;
	private String email;
	private String password;
	private int expmonth;
	private String expyear;
	private String cvv;
	private String query;
	private String textCompair;
	private String zip;
	private String address;
	private String mobilenumber;
	private String country;
	private String planname;
	private String shortdescription;
	private String subscriptionfee;
	private String securitycode;
	private String currency;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(final String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(final String password) {
		this.password = password;
	}
	
	public String getLoginUserName() {
		return loginUserName;
	}
	
	public void setLoginUserName(final String loginUserName) {
		this.loginUserName = loginUserName;
	}
	
	public String getLoginPassWord() {
		return loginPassWord;
	}
	
	public void setLoginPassWord(final String loginPassWord) {
		this.loginPassWord = loginPassWord;
	}

	public String getLoginName() {
		return loginName;
	}
	
	public void setLoginName(final String loginName) {
		this.loginName = loginName;
	}
	
	private String cardnumber;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public int getExpmonth() {
		return expmonth;
	}

	public void setExpmonth(int expmonth) {
		this.expmonth = expmonth;
	}

	public String getExpyear() {
		return expyear;
	}

	public void setExpyear(String expyear) {
		this.expyear = expyear;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getTextCompair() {
		return textCompair;
	}

	public void setTextCompair(String textCompair) {
		this.textCompair = textCompair;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobilenumber() {
		return mobilenumber;
	}

	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPlanname() {
		return planname;
	}

	public void setPlanname(String planname) {
		this.planname = planname;
	}

	public String getShortdescription() {
		return shortdescription;
	}

	public void setShortdescription(String shortdescription) {
		this.shortdescription = shortdescription;
	}

	public String getSubscriptionfee() {
		return subscriptionfee;
	}

	public void setSubscriptionfee(String subscriptionfee) {
		this.subscriptionfee = subscriptionfee;
	}

	public String getSecuritycode() {
		return securitycode;
	}

	public void setSecuritycode(String securitycode) {
		this.securitycode = securitycode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        return stringBuilder.append("loginUserName : " + loginUserName +
        		" loginPassWord : " + loginPassWord + " loginName : " + loginName +
        		" name : " + name +" email : " + email +
        		" password : " + password +
        		" ]").toString();
    }

}
