package com.seleniumtests.tests;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.seleniumtests.core.Filter;
import com.seleniumtests.core.SeleniumTestPlan;
import com.seleniumtests.dataobject.PPVData;
import com.seleniumtests.dataobject.SubscriptionData;
import com.seleniumtests.util.SpreadSheetHelper;
import com.seleniumtests.util.internal.entity.TestEntity;
import com.seleniumtests.webpage.PPVPage;
import com.seleniumtests.webpage.SubscriptionPage;

public class PPVTest extends SeleniumTestPlan {
	
	@DataProvider(

			name = "PPVData",
			parallel = false
			)
			public static Iterator < Object[] > getUserInfo(final Method m) throws Exception {


			Filter filter = Filter.equalsIgnoreCase(TestEntity.TEST_METHOD, m.getName());


			LinkedHashMap < String, Class < ? >> classMap = new LinkedHashMap < String, Class < ? >> ();
			classMap.put("TestEntity", TestEntity.class);
			classMap.put("PPVData", PPVData.class);

			return SpreadSheetHelper.getEntitiesFromSpreadsheet(PPVTest.class, classMap, "c1.csv", filter);
			}
			
			@Test(
		            groups = { "sanity", "regression" },
		            dataProvider = "PPVData",
		            description = "Play the Content through the PPV in Frontend"
		        )
		    
		        public void TC_PPVC_01(final TestEntity testEntity,final PPVData ppvData) throws Exception {

		            new PPVPage(true).TC_PPV_01(ppvData);
		            
		        }

}
