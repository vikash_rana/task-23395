/**
 * @author  Vikash Rana <vikash@muvi.com>
 */

package com.seleniumtests.webpage;

import static com.seleniumtests.core.Locator.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;
import static com.seleniumtests.core.CustomAssertion.*;
import static com.seleniumtests.core.Locator.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.lang.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.github.javafaker.Faker;
import com.google.gdata.util.parser.Action;
import com.seleniumtests.apicontroller.RestExecutor;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.DbManager;
import com.seleniumtests.dataobject.DbManager2;
import com.seleniumtests.dataobject.EndUserData;
import com.seleniumtests.dataobject.PPVData;
import com.seleniumtests.dataobject.BillingData;
import com.seleniumtests.dataobject.ContentData;
import com.seleniumtests.dataobject.RandomAddress;
import com.seleniumtests.dataobject.RandomEmailID;
import com.seleniumtests.dataobject.RandomMobileNo;
import com.seleniumtests.dataobject.RandomName;
import com.seleniumtests.dataobject.RandomPassword;
import com.seleniumtests.dataobject.RandomZip;
import com.seleniumtests.dataobject.SubscriptionData;
import com.seleniumtests.dataobject.WebCRMData;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.Table;
import com.seleniumtests.webelements.TextFieldElement;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.GetCurrentUrl;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mysql.jdbc.Driver;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.ContentData;

import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;

public class PPVPage extends PageObject {

	public static String url = null;
	public static String URL = null;
	private static final String APIURL = "https://mailsac.com/api/addresses/";	
	public static Map<Object,Map<Object,Object>> mastermap = new HashMap<Object, Map<Object,Object>>();
	
	public PPVPage(final boolean openPageURL) throws Exception {
		
	    super(htmlpage, openPageURL ? url = SeleniumTestsContextManager.getThreadContext().getAppURL() : null);
	    
	}
	
	private static final ButtonElement htmlpage = new ButtonElement("End User should be able to purchase a content via PPV",
			  locateByXPath("//html"));
	
	/* Muvi.com */
	private static final ButtonElement Login = new ButtonElement("Click login from Homepage",locateByXPath("//a[contains(@class,'_login')]"));
	
	/* Login on Muvi.com */
	private static final TextFieldElement LoginID = new TextFieldElement("Enter the Login ID",locateByXPath("//input[contains(@id,'LoginForm_email')]"));
	private static final TextFieldElement LoginPswd = new TextFieldElement("Enter the Login Password",locateByXPath("//input[contains(@id,'LoginForm_password')]"));
	private static final ButtonElement LoginButton = new ButtonElement("Click Login Button",locateByXPath("//input[contains(@id,'btn-login')]"));
	
	/* Preview the Website From CMS */
	private static final ButtonElement PreviewButton = new ButtonElement("Click Preview from CMS",locateByXPath("//a[contains(@id,'prev_btn_disable')]"));
	
	/* Customer's Website : Modern */
	private static final ButtonElement WebsiteLogin = new ButtonElement("Click Login to redirect Login page",locateByXPath("//a[contains(@class,'login muvi_c-link')]"));
	
	/* EndUser Login Credentials */
	private static final TextFieldElement WebsiteLoginID = new TextFieldElement("Enter Email in Login page",locateByXPath("//div[@class='row']//input[@id='username']"));
	private static final TextFieldElement WebsiteLoginPswd = new TextFieldElement("Enter Password in Login page",locateByXPath("//div[@class='row']//input[@id='password']"));
	private static final ButtonElement WebsiteLoginButton = new ButtonElement("Click Login in Login page",locateByXPath("//div[@class='row']//button[contains(@name,'login_submit')]"));
	
	/* Redirect to Movie List Page */
	private static final ButtonElement WebsiteMenu = new ButtonElement("Click Menu in Homepage",locateByXPath("//a[contains(text(),'Movie')]"));
	
	/* Redirect to Detail Page */
	private static final ButtonElement MenuContent1 = new ButtonElement("Click on Play of Content 1",locateByXPath("//a[contains(text(),'Black Panther')]"));
	private static final ButtonElement PlayNow = new ButtonElement("Click on Play to Content 1",locateByXPath("//a[contains(@class,'playbtn')]"));
	
	/* Buy Content through PPV */
	private static final ButtonElement ProceedToPayment = new ButtonElement("Click on Proceed to Payment Button on PPV popup",locateByXPath("//button[contains(@id,'btn_proceed_payment')]"));
	private static final TextFieldElement NameOnCard = new TextFieldElement("Enter the Card Name",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'card_name')]"));
	private static final TextFieldElement CardNumber = new TextFieldElement("Enter the Card Number",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'card_number')]"));
	private static final TextFieldElement SecurityNumber = new TextFieldElement("Enter the Card Security Number",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'security_code')]"));
	private static final ButtonElement PayNow = new ButtonElement("Click on Pay Now Button on PPV popup",locateByXPath("//button[@id='paynowbtn']"));

	/* For the Month & Year */
	String ExpMonth = "//select[@id='exp_month']";
	String ExpYear = "//select[@id='exp_year']";
	String Country = "//select[@id='countr']";
	String CurrencyDropdown = "//select[@id='currency-box']";
	String DurationList = "//select[@name='data[recurrence]']";
	
	public PPVPage Textbox(TextFieldElement element,final String Text) throws Exception {
		 waitForSeconds(2);
		 element.clearAndType(Text);
		 return this;
	} 
	public PPVPage MouseOver(HtmlElement element) throws Exception {
		 element.waitForPresent(10);
		 element.mouseOver();
		 return this;
	} 
	public PPVPage Click(ButtonElement element) throws Exception {
		 element.waitForPresent(10);
		 element.click();
		 return this;
	}
	public PPVPage Click1(ButtonElement element) throws Exception {
		 waitForSeconds(10);
		 try {
		 	  element.click();
		 }catch(Exception e) {
		  		
		 }
		 return this;
	}
	public PPVPage SelectByText(final String Text, String xpath) throws Exception {

		 SelectList s = new SelectList(Text, By.xpath(xpath));
		 s.selectByText(Text);
		 return this;
	}
	public PPVPage Scroll(ButtonElement element) throws Exception {
		 waitForSeconds(2);
		 element.scroll();
		 return this;
	}
	public PPVPage SimulateClick(ButtonElement element) throws Exception {
		 waitForSeconds(2);
		 element.simulateClick();
		 return this;
	}
	public PPVPage HtmlvideoPlayerPlay(HtmlElement element) throws Exception {
		 waitForSeconds(2);
		 element.HtmlvideoPlayerPlay();
		 return this;
	}
	public PPVPage HtmlvideoPlayerPause(HtmlElement element) throws Exception {
		waitForSeconds(2);
		element.HtmlvideoPlayerPause();
		return this;
	}
	public PPVPage selectNewTab() throws Exception {
		waitForSeconds(2);
		selectNewWindow();
		return this;
	}
	public PPVPage SelectWindow(int index) throws Exception {
		waitForSeconds(2);
		selectWindow(index);
		return this;
	}
	public PPVPage printpagesource() throws Exception {
		waitForSeconds(2);
		System.out.println(driver.getPageSource());
		return this;
	}
	public PPVPage Wait(int value) throws Exception {
		 waitForSeconds(value);
		 return this;
	}
	public PPVPage WaitforElelment(HtmlElement element) throws Exception {
		 waitForElementPresent(element);
		 return this;    
	}
	public PPVPage SelectByList(final String Text,String xpath) throws Exception {
		 waitForSeconds(5);
		 SelectList s = new SelectList(Text, By.xpath(xpath));
		 s.selectByValue(Text);
		 return this;
	}
	public PPVPage SelectByListIndex(final int index,String xpath) throws Exception {
		 waitForSeconds(5);
		 SelectList s = new SelectList("", By.xpath(xpath));
		 s.selectByIndex(index);
		 return this;
	}
	public PPVPage AssertEquals(final String actualText,final String expectedText) throws Exception {
		 assertEquals(actualText.toLowerCase(),expectedText.toLowerCase(),"Actual text and Expected Text Not Equal");
		 return this;
	}
	public PPVPage AssertFalse(final Boolean actualText) throws Exception {
		 assertFalse(actualText,"Text should not be visible but now its visible");
		 return this;
	}
	public PPVPage AssertTrue(final Boolean actualText) throws Exception {
		 assertTrue(actualText,"Text should  be visible but now its not visible");
		 return this;
	}
	public PPVPage pressEnter() throws Exception {
		 Actions act=new Actions(driver);
		 act.sendKeys(Keys.ENTER).perform();
		 return this;
	}
	
	public PPVPage TC_PPV_01(final PPVData ppvData) throws Exception {
		 return Click(Login).Wait(2)
				.Textbox(LoginID,"Upgrade_muvi@yopmail.com").Wait(2)
				.Textbox(LoginPswd,"muvi1234#").Wait(2)
				.Click(LoginButton).Wait(5)
				.Click(PreviewButton).Wait(5)
				.selectNewTab()
				.Click(WebsiteLogin).Wait(5)
				.Textbox(WebsiteLoginID, "Muvi_Cou2@yopmail.com").Wait(2)
				.Textbox(WebsiteLoginPswd, "123456").Wait(2)
				.Click(WebsiteLoginButton).Wait(5)
				.Click(WebsiteMenu).Wait(5)
				.Click(MenuContent1).Wait(5)
				.Click(PlayNow).Wait(5)
				.Click(ProceedToPayment).Wait(5)
				.Textbox(NameOnCard, "Muvi").Wait(2)
				.Textbox(CardNumber, "4111111111111111").Wait(2)
				.SelectByText("MAR", ExpMonth).Wait(2)
				.SelectByText("2020", ExpYear).Wait(2)
				.Textbox(SecurityNumber, "123").Wait(2)
				.Click(PayNow).Wait(50);
		  }
}
