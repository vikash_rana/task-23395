# Task-23395

Automation|Enterprise onboarding sanity test
Checklist:
#1. End User should be able to register.
#2. End User should be able to purchase content via Subscription / PPV.
#3. End User should be able to purchase content by applying Coupon.