/**
 * @author  Vikash Rana <vikash@muvi.com>
 */

package com.seleniumtests.webpage;

import static com.seleniumtests.core.Locator.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;
import static com.seleniumtests.core.CustomAssertion.*;
import static com.seleniumtests.core.Locator.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.lang.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.github.javafaker.Faker;
import com.google.gdata.util.parser.Action;
import com.seleniumtests.apicontroller.RestExecutor;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.DbManager;
import com.seleniumtests.dataobject.DbManager2;
import com.seleniumtests.dataobject.EndUserData;
import com.seleniumtests.dataobject.BillingData;
import com.seleniumtests.dataobject.ContentData;
import com.seleniumtests.dataobject.RandomAddress;
import com.seleniumtests.dataobject.RandomEmailID;
import com.seleniumtests.dataobject.RandomMobileNo;
import com.seleniumtests.dataobject.RandomName;
import com.seleniumtests.dataobject.RandomPassword;
import com.seleniumtests.dataobject.RandomZip;
import com.seleniumtests.dataobject.SubscriptionData;
import com.seleniumtests.dataobject.WebCRMData;
import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.Table;
import com.seleniumtests.webelements.TextFieldElement;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.GetCurrentUrl;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mysql.jdbc.Driver;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.seleniumtests.dataobject.ContentData;

import com.seleniumtests.webelements.ButtonElement;
import com.seleniumtests.webelements.CheckBoxElement;
import com.seleniumtests.webelements.HtmlElement;
import com.seleniumtests.webelements.PageObject;
import com.seleniumtests.webelements.SelectList;
import com.seleniumtests.webelements.TextFieldElement;

public class SubscriptionPage extends PageObject {

	public static boolean booleanvalue = false;

	 public static String url = null;
	 public static String URL = null;
	 private static final String APIURL = "https://mailsac.com/api/addresses/";
	 public static Map < Object, Map < Object, Object >> mastermap = new HashMap < Object, Map < Object, Object >> ();
	 public SubscriptionPage(final boolean openPageURL) throws Exception {
	  super(htmlpage, openPageURL ? url = SeleniumTestsContextManager.getThreadContext().getAppURL() : null);
	 }

	 private static final ButtonElement htmlpage = new ButtonElement("Click login button ",
			  locateByXPath("//html"));
	 
	 private static final ButtonElement LoginButton = new ButtonElement("Click login button ",
			  locateByXPath("//a[@class='_login']"));
	 private static final TextFieldElement LoginUserNameTextbox = new TextFieldElement("enter username in User name textbox ",
			  locateByXPath("//input[@id='LoginForm_email']"));
	 private static final TextFieldElement PasswordNameTextbox = new TextFieldElement("enter password in password textbox ",
			  locateByXPath("//input[@id='LoginForm_password']"));
	 private static final ButtonElement LoginButton1 = new ButtonElement("Click login button ",
			  locateByXPath("//input[@id='btn-login']"));
	 
	 private static final ButtonElement PreviewButton = new ButtonElement("Click Preview from CMS ",
		       locateByXPath("//a[contains(text(),'Preview Website')]"));
	 
	 private static final HtmlElement SubscriptionOver = new HtmlElement("Click Preview from CMS ",
		       locateByXPath("//em[@class='fa fa-money left-icon']")); //a[contains(text(),'Monetization')]
	 private static final ButtonElement Subscription = new ButtonElement("Click Preview from CMS ",
		       locateByXPath("//a[@href='/subscriptions/subscriptions']"));
	 private static final ButtonElement AddSubscription = new ButtonElement("Click Preview from CMS ",
		       locateByXPath("//button[contains(text(),'Add')]"));
	 private static final TextFieldElement AddSubscriptionName = new TextFieldElement("Click Preview from CMS ",
		       locateByXPath("//input[@placeholder='Plan Name']"));
	 private static final TextFieldElement AddSubscriptionDesc = new TextFieldElement("Click Preview from CMS ",
		       locateByXPath("//textarea[@id='short_desc']"));
	 private static final TextFieldElement Charge = new TextFieldElement("Select FromList",
		       locateByXPath("//div[@class='modal-body']//input[@placeholder='Subscription Fee']"));
	 private static final ButtonElement Submit = new ButtonElement("Click Preview from CMS ",
		       locateByXPath("//button[@id='sub-btn']"));
	 
//	 //Redirect to Detail Page
//	 private static final ButtonElement WebsiteMenu = new ButtonElement("Click Menu in Homepage",locateByXPath("//a[contains(text(),'Movie')]"));
//	 private static final ButtonElement MenuContent1 = new ButtonElement("Click on Play of Content 1",locateByXPath("//a[contains(text(),'Black Panther')]"));
//	 private static final ButtonElement PlayNow = new ButtonElement("Click on Play to Content 1",locateByXPath("//a[contains(@class,'playbtn')]"));
//	 
//	 //Register Through popup
//	 private static final ButtonElement Register = new ButtonElement("Click on the Register From popup",locateByXPath("//div[contains(@class,'pull-left')]//a[contains(@class,'link-ylw txt-right')]"));
//	 private static final TextFieldElement EndUserName = new TextFieldElement("Enter the Full Name",locateByXPath("//input[contains(@class,'form-control field-noscript')]"));
//	 private static final TextFieldElement EndUserEmail = new TextFieldElement("Enter the Email",locateByXPath("//input[contains(@id,'email_address')]"));
//	 private static final TextFieldElement EndUserPass = new TextFieldElement("Enter password in Password",locateByXPath("//input[contains(@id,'join_password')]"));
//	 private static final TextFieldElement EndUserCnfm = new TextFieldElement("Enter password in Confirm Password",locateByXPath("//input[contains(@id,'confirm_password')]"));
//	 private static final ButtonElement RegisterButton = new ButtonElement("Click Register button",locateByXPath("//button[contains(text(),'SIGNUP')]"));
//	 
//	 //Click on Proceed to Payment 
//	 private static final ButtonElement ProceedToPayment = new ButtonElement("Click on Proceed to Payment",locateByXPath("//button[contains(@id,'btn_proceed_payment')]"));
//	 
//	 //Card Details
//	 private static final TextFieldElement NameOnCard = new TextFieldElement("Enter the Card Name",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'card_name')]"));
//	 private static final TextFieldElement CardNumber = new TextFieldElement("Enter the Card Number",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'card_number')]"));
//	 private static final TextFieldElement SecurityNumber = new TextFieldElement("Enter the Card Security Number",locateByXPath("//div[contains(@id,'card_detail_div')]//input[contains(@id,'security_code')]"));
//	 private static final ButtonElement PayNow = new ButtonElement("Click on Pay Now Button on PPV popup",locateByXPath("//button[@id='paynowbtn']"));//div[contains(@class,'muvi_c-navbar')]
	 
	 //Play the Content
	 private static final ButtonElement MenuNav = new ButtonElement("Click Menu in Homepage",locateByXPath("//a[contains(text(),'Movie')]"));
	 private static final ButtonElement MenuContent2 = new ButtonElement("Click on Play of Content 1",locateByXPath("//a[contains(text(),'Black Panther')]"));
	 private static final ButtonElement PlayButton = new ButtonElement("Click on Play to Content 1",locateByXPath("//a[contains(@class,'playbtn')]"));
	 
	 //Register through Page
	 private static final ButtonElement RegisterNav = new ButtonElement("Click Register in Homepage",locateByXPath("//a[contains(@class,'register muvi_c-link')]"));
	 private static final TextFieldElement UserName = new TextFieldElement("Enter the Full Name",locateByXPath("//div[@class='col-sm-4']//input[@id='name']"));
	 private static final TextFieldElement UserEmail = new TextFieldElement("Enter the Email",locateByXPath("//div[@class='col-sm-4']//input[@id='email_address']"));
	 private static final TextFieldElement UserPass = new TextFieldElement("Enter password in Password",locateByXPath("//div[@class='col-sm-4']//input[@id='join_password']"));
	 private static final TextFieldElement UserCnfm = new TextFieldElement("Enter password in Confirm Password",locateByXPath("//div[@class='col-sm-4']//input[@id='confirm_password']"));
	
	 private static final TextFieldElement CardName = new TextFieldElement("Enter the Card Name",locateByXPath("//input[@id='card_name']"));
	 private static final TextFieldElement CardNumbers = new TextFieldElement("Enter Card Number",locateByXPath("//input[@id='card_number']"));
	 private static final TextFieldElement SecurityCode = new TextFieldElement("Enter password in Confirm Password",locateByXPath("//input[@id='security_code']"));
	 private static final ButtonElement UserRegister = new ButtonElement("Click Register button",locateByXPath("//button[@id='register_membership']"));
	 	 
	 String ExpMonth = "//select[@id='exp_month']";
	 String ExpYear = "//select[@id='exp_year']";
	 String Country = "//select[@id='countr']";
	 String CurrencyDropdown = "//select[@id='currency-box']";
	 String DurationList = "//select[@name='data[recurrence]']";
	 
	 		
	 public SubscriptionPage Textbox(TextFieldElement element,final String Text) throws Exception {
	 	waitForSeconds(2);
	 	element.clearAndType(Text);
	     return this;
	 } 
	 public SubscriptionPage MouseOver(HtmlElement element) throws Exception {
	 	
	 	element.waitForPresent(10);
	 	element.mouseOver();
	     return this;
	 } 
	  
	 public SubscriptionPage Click(ButtonElement element) throws Exception {
	 	   
	 	   element.waitForPresent(10);
	 	   element.click();
	         return this;
	     }
	  public SubscriptionPage Click1(ButtonElement element) throws Exception {
	 	 waitForSeconds(10);
	  	try {
	 	    element.click();
	  	}catch(Exception e) {
	  		
	  	}
	      return this;
	  }
	  
	  public SubscriptionPage SelectByText(final String Text, String xpath) throws Exception {

		  SelectList s = new SelectList(Text, By.xpath(xpath));
		  s.selectByText(Text);
		  return this;
	  }
	  
	  public SubscriptionPage Scroll(ButtonElement element) throws Exception {
	 	 waitForSeconds(2);
	 	    element.scroll();
	      return this;
	  }
	  
	  
	  public SubscriptionPage SimulateClick(ButtonElement element) throws Exception {
	 	 waitForSeconds(2);
	 	    element.simulateClick();
	      return this;
	  }
	  public SubscriptionPage HtmlvideoPlayerPlay(HtmlElement element) throws Exception {
	 	 waitForSeconds(2);
	 	    element.HtmlvideoPlayerPlay();
	      return this;
	  }
	  public SubscriptionPage HtmlvideoPlayerPause(HtmlElement element) throws Exception {
	 	 waitForSeconds(2);
	 	    element.HtmlvideoPlayerPause();
	      return this;
	  }
	  
	  public SubscriptionPage selectNewTab() throws Exception {
	 	 waitForSeconds(2);
	  	selectNewWindow();
	 	return this;
	 	
	      
	  }
	  
	  public SubscriptionPage SelectWindow(int index) throws Exception {
	 	 waitForSeconds(2);
	 	 	selectWindow(index);
	 		return this;
	 	     
	 	 }
	  
	  public SubscriptionPage printpagesource() throws Exception {
	 	 waitForSeconds(2);
	 	 	System.out.println(driver.getPageSource());
	 		return this;
	  }
	  public SubscriptionPage Wait(int value) throws Exception {
	 	 waitForSeconds(value);
	 	 	
	 		return this;
	 	     
	 	 }
	  public SubscriptionPage WaitforElelment(HtmlElement element) throws Exception {
	 	 waitForElementPresent(element);
	 		return this;    
	 	 }
	  public SubscriptionPage SelectByList(final String Text,String xpath) throws Exception {
	 	 waitForSeconds(5);
	 	SelectList s = new SelectList(Text, By.xpath(xpath));
	 	s.selectByValue(Text);
	 		return this;
	 	     
	 	 }
	  public SubscriptionPage SelectByListIndex(final int index,String xpath) throws Exception {
	 	 waitForSeconds(5);
	 		SelectList s = new SelectList("", By.xpath(xpath));
	 		s.selectByIndex(index);
	 			return this;
	 	 }
	  public SubscriptionPage AssertEquals(final String actualText,final String expectedText) throws Exception {
			 assertEquals(actualText.toLowerCase(),expectedText.toLowerCase(),"Actual text and Expected Text Not Equal");
		     return this;
		 }
	  public SubscriptionPage AssertFalse(final Boolean actualText) throws Exception {
			 assertFalse(actualText,"Text should not be visible but now its visible");
		     return this;
	     }
	  public SubscriptionPage AssertTrue(final Boolean actualText) throws Exception {
			 assertTrue(actualText,"Text should  be visible but now its not visible");
		     return this;
		 }
	  public SubscriptionPage pressEnter() throws Exception {
			 Actions act=new Actions(driver);
			 act.sendKeys(Keys.ENTER).perform();
		     return this;
		 }
	 
//	  public SubscriptionPage TC_Subscription_02(final SubscriptionData subscriptionData) throws Exception {
//			 return Click(LoginButton).Wait(2)
//					.Textbox(LoginUserNameTextbox,"Upgrade_muvi@yopmail.com").Wait(2)
//					.Textbox(PasswordNameTextbox,"muvi1234#").Wait(2)
//					.Click(LoginButton1).Wait(5)
//					.MouseOver(SubscriptionOver).Wait(5)
//					.Click(Subscription).Wait(5)
//					.Click(AddSubscription).Wait(5)
//					.Textbox(AddSubscriptionName,"Tur_SUB").Wait(5)
//					.Textbox(AddSubscriptionDesc,"Enjoy the Movie..!!").Wait(5)
//					.SelectByText("Quarterly", DurationList).Wait(2)
//					.Textbox(Charge, "100").Wait(2)
//					.Click(Submit).Wait(5)
//					.Click(PreviewButton).Wait(5)
//					.selectNewTab()
//					.Click(WebsiteMenu).Wait(5)
//					.Click(MenuContent1).Wait(5)
//					.Click(PlayNow).Wait(5)
//					.Click(Register).Wait(5)
//					.Textbox(EndUserName, "Muvi").Wait(2)
//					.Textbox(EndUserEmail, "Muvi_Abcdef@yopmail.com").Wait(2)
//					.Textbox(EndUserPass, "123456").Wait(2)
//					.Textbox(EndUserCnfm, "123456").Wait(2)
//					.Click(RegisterButton).Wait(5)
//					.Click(ProceedToPayment).Wait(5)
//					.Textbox(NameOnCard, "Muvi").Wait(2)
//					.Textbox(CardNumber, "4111111111111111").Wait(2)
//					.SelectByText("MAR", ExpMonth).Wait(2)
//					.SelectByText("2020", ExpYear).Wait(2)
//					.Textbox(SecurityNumber, "123").Wait(2)
//					.Click(PayNow).Wait(5)
//					.Click(MenuNav).Wait(5)
//					.Click(MenuContent2).Wait(5)
//					.Click(PlayButton).Wait(30);
//			  }
	  
	  public SubscriptionPage TC_Subscription_02(final SubscriptionData subscriptionData) throws Exception {
		  
		  return Click(LoginButton).Wait(2)
				  .Textbox(LoginUserNameTextbox,"Upgrade_muvi@yopmail.com").Wait(2)
				  .Textbox(PasswordNameTextbox,"muvi1234#").Wait(2)
				  .Click(LoginButton1).Wait(5)
				  .MouseOver(SubscriptionOver).Wait(5)
				  .Click(Subscription).Wait(5)
				  .Click(AddSubscription).Wait(5)
				  .Textbox(AddSubscriptionName,"Tur_SUB").Wait(5)
				  .Textbox(AddSubscriptionDesc,"Enjoy the Movie..!!").Wait(5)
				  .SelectByText("Quarterly", DurationList).Wait(2)
				  .Textbox(Charge, "100").Wait(2)
				  .Click(Submit).Wait(5)
				  .Click(PreviewButton).Wait(5)
				  .selectNewTab()
				  .Click(RegisterNav).Wait(5)
				  .Textbox(UserName,"Muvi").Wait(5)
				  .Textbox(UserEmail,"Muvi_Muvi_01@yopmail.com").Wait(5)
				  .Textbox(UserPass,"123456").Wait(5)
				  .Textbox(UserCnfm,"123456").Wait(5)
				  .Textbox(CardName,"Muvi").Wait(5)
				  .Textbox(CardNumbers,"4111111111111111").Wait(5)
				  .SelectByText("MAR", ExpMonth).Wait(2)
				  .SelectByText("2020", ExpYear).Wait(2)
				  .Textbox(SecurityCode,"123").Wait(5)
				  .Click(UserRegister).Wait(50)
				  .Click(MenuNav).Wait(5)
				  .Click(MenuContent2).Wait(5)
				  .Click(PlayButton).Wait(5)
				  ;
	  }
}
