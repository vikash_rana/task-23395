package com.seleniumtests.tests;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.seleniumtests.core.Filter;
import com.seleniumtests.core.SeleniumTestPlan;
import com.seleniumtests.dataobject.SubscriptionData;
import com.seleniumtests.util.SpreadSheetHelper;
import com.seleniumtests.util.internal.entity.TestEntity;
import com.seleniumtests.webpage.SubscriptionPage;

public class SubscriptionTest extends SeleniumTestPlan{

	@DataProvider(

			name = "SubscriptionData",
			parallel = false
			)
			public static Iterator < Object[] > getUserInfo(final Method m) throws Exception {


			Filter filter = Filter.equalsIgnoreCase(TestEntity.TEST_METHOD, m.getName());


			LinkedHashMap < String, Class < ? >> classMap = new LinkedHashMap < String, Class < ? >> ();
			classMap.put("TestEntity", TestEntity.class);
			classMap.put("SubscriptionData", SubscriptionData.class);

			return SpreadSheetHelper.getEntitiesFromSpreadsheet(SubscriptionTest.class, classMap, "c1.csv", filter);
			}
			
			@Test(
		            groups = { "sanity", "regression" },
		            dataProvider = "SubscriptionData",
		            description = "Verify the All content subscription plan in CMS"
		        )
		    
		        public void TC_Subscription_01(final TestEntity testEntity,final SubscriptionData SubscriptionData) throws Exception {

		            new SubscriptionPage(true).TC_Subscription_02(SubscriptionData);
		            
		        }
}
