package com.seleniumtests.tests;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.seleniumtests.core.Filter;
import com.seleniumtests.core.SeleniumTestPlan;
import com.seleniumtests.dataobject.CouponData;
import com.seleniumtests.dataobject.PPVData;
import com.seleniumtests.util.SpreadSheetHelper;
import com.seleniumtests.util.internal.entity.TestEntity;
import com.seleniumtests.webpage.CouponPage;
import com.seleniumtests.webpage.PPVPage;

public class CouponTest extends SeleniumTestPlan {

	@DataProvider(

			name = "CouponData",
			parallel = false
			)
			public static Iterator < Object[] > getUserInfo(final Method m) throws Exception {


			Filter filter = Filter.equalsIgnoreCase(TestEntity.TEST_METHOD, m.getName());


			LinkedHashMap < String, Class < ? >> classMap = new LinkedHashMap < String, Class < ? >> ();
			classMap.put("TestEntity", TestEntity.class);
			classMap.put("CouponData", CouponData.class);

			return SpreadSheetHelper.getEntitiesFromSpreadsheet(CouponTest.class, classMap, "c1.csv", filter);
			}
			
			@Test(
		            groups = { "sanity", "regression" },
		            dataProvider = "CouponData",
		            description = "Play the Content through the PPV in Frontend"
		        )
		    
		        public void TC_COUP_01(final TestEntity testEntity,final CouponData CouponData) throws Exception {

		            new CouponPage(true).TC_Cou_01(CouponData);
		            
		        }
	
	
}
